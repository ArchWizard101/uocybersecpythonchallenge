#!/usr/bin/env python3
# https://codezup.com/socket-server-with-multiple-clients-model-multithreading-python/

import socket
import os
import pickle
from _thread import *

ServerSocket = socket.socket()
host = "127.0.0.1"
port = 9001
ThreadCount = 0

try:
    ServerSocket.bind((host, port))
except socket.error as e:
    print(str(e))

print("Waiting for a Connection..")
ServerSocket.listen(5)


def threaded_client(connection):
    while True:
        data = connection.recv(2048)
        data = pickle.loads(data)
        if not data:
            break
        if data["command"] == "echo":
            reply = "Server Says: " + data["content"]
            connection.sendall(pickle.dumps({"command": "echo", "content": reply}))
        elif data["command"] == "print":
            print("Client Says: " + data["content"])
    connection.close()


while True:
    Client, address = ServerSocket.accept()
    print("Connected to: " + address[0] + ":" + str(address[1]))
    start_new_thread(threaded_client, (Client,))
    ThreadCount += 1
    print("Thread Number: " + str(ThreadCount))
ServerSocket.close()
