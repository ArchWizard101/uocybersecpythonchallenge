#!/usr/bin/env python3

import socket
import cmd
import pickle


class EchoClient(cmd.Cmd):
    intro = "Welcome to the Echo client.   Type help or ? to list commands.\n"
    prompt = "-> "

    def do_echo(self, arg):
        "send an echo command to the server and print the response"
        try:
            self.s.sendall(pickle.dumps({"command": "echo", "content": str(arg)}))
            data = self.s.recv(2048)
            data = pickle.loads(data)
            if data["command"] == "echo":
                print(data["content"])
        except:
            print("an error occurred. have you connected?")

    def do_print(self, arg):
        "send something to the server to print"
        try:
            self.s.sendall(pickle.dumps({"command": "print", "content": str(arg)}))
            print("Sent")
        except:
            print("an error occurred. have you connected?")

    def do_bye(self, arg):
        "Close the socket and exit the program"
        print("Thank you for using the Echo client")
        try:
            self.s.close()
            self.close()
        except:
            print("An error occurred")
        return True

    def do_connect(self, arg):
        """connect <ip> <port>
        Connect to an echo server
        """
        args = arg.split()
        if len(args) != 2:
            print("Error, missing args, try help")
            return
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.connect((args[0], int(args[1])))
        except:
            print("An error occurred")


if __name__ == "__main__":
    EchoClient().cmdloop()
